#!/bin/bash

read number_count
sum=0

for ((i = 1; i <= number_count; i++))
do
    read numbers
    sum=$((sum+numbers))
done

echo $sum/$number_count | bc -l | xargs printf "%.3f\n" 
